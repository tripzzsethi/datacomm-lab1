# README #


### What is this repository for? ###

* srget is a simple Python HTTP downloader. The features it supports are shown below.
* (https://bitbucket.org/tripzzsethi/datacomm-lab1)



### How do I get set up? ###

* to run the program  ( ./srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file  )
where -c is the # of connection and optional.
* HTTP GET - First, get defined URL and check for the content length.
* Multiple segment file downloading - Multiple segment file downloading use multiple thread to download the separated part of the URL file, you can simply give two arguments: URL address and the segment number.
* Redirection - gets the new address/location from the header and issues new connection to proceed with the download process.



### Features ###

HTTP GET
multi-threaded fetch from the URL
multi-segment file fetch
download pause and resume
change # of thread while resume
support redirection
support URL with no Content-Length



### Who do I talk to? ###

* https://bitbucket.org/tripzzsethi/datacomm-lab1
* email - tripta.tripzz05@gmail.com